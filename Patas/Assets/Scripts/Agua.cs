﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agua : Bala
{
    // Start is called before the first frame update
    private Bala agua;
    void Start()
    {
        agua = GetComponent<Bala>();
        agua.cambiarDaño(30);
        agua.cambiarVelocidad(10f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

