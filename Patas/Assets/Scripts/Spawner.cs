﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemigo;
    // Start is called before the first frame update
    public float speed;
    void Start()
    {
        speed = Random.Range (10f, 50f);
        InvokeRepeating("Spawn", Random.Range(2f,20f), speed);
    }

    void Spawn()
    {
        Instantiate(enemigo, transform.position,Quaternion.identity);
        speed = Random.Range (10f, 50f);
    }
    void Update()
    {
        
    }
}