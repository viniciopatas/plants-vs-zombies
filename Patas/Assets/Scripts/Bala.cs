﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{   
    public Rigidbody2D rb;
    private Vector3 fuerza;
    public int daño;
    public float velocidad;
    // Start is called before the first frame update
    void Start()
    {
        velocidad = 6f;
        fuerza = new Vector3 (10f,0f,0f);
    }
    public void PonerRB(Rigidbody2D rigbod){
        rb = rigbod;
    }
    public int MostrarDaño(){
        return daño;
    }
    public float speed(){
        return velocidad;
    }
    public void cambiarVelocidad(float newVelocidad){
        velocidad = newVelocidad;
    }
    public void cambiarDaño(int newDaño){
        daño = newDaño;
    }
    void Update()
    {
        
        rb.AddForce(fuerza * 0.4f);
    }
    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
