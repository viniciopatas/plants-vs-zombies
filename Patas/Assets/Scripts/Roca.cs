﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roca : Bala
{
    // Start is called before the first frame update
    private Bala roca;
    private Rigidbody2D rigbody;
    void Start()
    {
        
        rigbody = GetComponent <Rigidbody2D>();
        roca.PonerRB(rigbody);
        roca = GetComponent<Bala>();
        roca.cambiarDaño(75);
        roca.cambiarVelocidad(7f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
