﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed; 
    static int vida;
    void Start()
    {
        vida = 200;
        speed = 0.7f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Bala"){
            Bala disparo;
            disparo = other.gameObject.GetComponent<Bala>();
            SufrirDaño(disparo);
            Destroy(other.gameObject);
        }
    }
    void SufrirDaño(Bala disparo)
    {
        vida -= disparo.MostrarDaño();
        Debug.Log(vida);
        if (vida <= 0){
            Destroy(gameObject);
        }
    }
    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
