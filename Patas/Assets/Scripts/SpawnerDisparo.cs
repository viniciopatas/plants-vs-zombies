﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerDisparo : MonoBehaviour
{
    // Start is called before the first frame update
    public Bala velocidad;
    public GameObject bala;
    void Start()
    {
        velocidad = GetComponent <Bala>();
        InvokeRepeating("disparar", 3f, velocidad.speed());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void disparar(GameObject bala)
    {
        Instantiate(bala, transform.position,transform.rotation);
    }
}
